<!DOCTYPE html>
<html>

<?php include(dirname(__DIR__) . '/head.html') ?>

<body>
<?php include(dirname(__DIR__) . '/navbar.html'); ?>

<div class="container">
    <div class="row">
        <h1>UPLOAD</h1>

        <?php foreach ($message as $item): ?>
            <div><?= $item ?></div>
        <?php endforeach; ?>

        <div class="col-md-12">
        <form action="?page=upload" method="POST" ENCTYPE="multipart/form-data">
            <input type="file" name="file"/><br/>
            <input type="submit" value="send"/>
        </form>
        </div>
    </div>
</div>
</body>
</html>