<?php

class UploadController extends AppController
{
    const MAX_FILE_SIZE = 1024*1024;
    const SUPPORTED_TYPES = ['text/txt', 'text/rtf', 'text/doc', 'text/docx', 'text/odt', 'text/css', 'text/htm', 'text/html', 'text/xml',];

    private $message = [];

    public function __construct()
    {
        parent::__construct();
    }

    public function upload()
    {
        if ($this->isPost() && is_uploaded_file($_FILES['file']['tmp_name']) && $this->validate($_FILES['file'])) {

            move_uploaded_file(
                $_FILES['file']['tmp_name'],
                dirname(__DIR__).self::UPLOAD_DIRECTORY.$_FILES['file']['name']
            );

            //todo set data to DB
            //podlaczyc require once FileMapper
            //polaczyc require once File

            // $mapper = new FileMapper();
            // $file = new File(date("Y-m-d h:i:sa"), $_FILES['file']['name'], $_FILES['file']['size']);
            //$mapper->setFile($file);

            $this->message[] = 'File uploaded.';
        }

        $this->render('upload', [ 'message' => $this->message]);
    }

    private function validate(array $file): bool
    {
        if ($file['size'] > self::MAX_FILE_SIZE) {
            $this->message[] = 'File is too large for destination file system.';
            return false;
        }

        if (!isset($file['type']) || !in_array($file['type'], self::SUPPORTED_TYPES)) {
            $this->message[] = 'File type is not supported.';
            return false;
        }

        return true;
    }
}
