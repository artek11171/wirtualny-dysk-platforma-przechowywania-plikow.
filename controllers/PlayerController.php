<?php

class PlayerController extends AppController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function player()
    {
        if ($this->isPost())
        {
            $attachment_location = $_POST['filePath'];
            $pathinfo = pathinfo($attachment_location);
            $fileName = $pathinfo['basename'];

            header($_SERVER["SERVER_PROTOCOL"] . " 200 OK");
            header("Cache-Control: public"); // needed for internet explorer
            header("Content-Type: application/zip");
            header("Content-Transfer-Encoding: Binary");
            header("Content-Length:".filesize($attachment_location));
            header("Content-Disposition: attachment; filename=".$fileName);
            readfile($attachment_location);
            die();

        exit;
        }

        $this->render('player', [ 'videos' => $this->getVideos()]);
    }

    private function getNotHidden(array $files) {
        foreach($files as $key=>$file) {
            if ($file[0] === '.') {
                unset($files[$key]);
            };

        }
        return $files;
    }
    private function getVideos(): array
    {
        $files = scandir(dirname(__DIR__) . self::UPLOAD_DIRECTORY, SCANDIR_SORT_NONE);

        return $this->getNotHidden($files);
    }

}